db.products.insertMany([

    {
        name: "Iphone X",
        price: 30000,
        isActive: true
    },
    {
        name: "Samsung Galaxy S21",
        price: 51000,
        isActive: true
    },
    {
        name: "Razer Blackshark V2X",
        price: 2800,
        isActive: false
    },
    {
        name: "RAKK Gaming Mouse",
        price: 1800,
        isActive: true
    },
    {
        name: "Razer Mechanical Keyboard",
        price: 4000,
        isActive: true

    }
])

//Query Operators
//Allow us to expand our queries and define conditions
//looking for specific values

//$gt - greater than
//db.products.find({price:{$gt:3000}})

//$lt - less than
//db.products.find({price:{$lt:3000}})

//$gte - less than
//db.products.find({price:{$gte:30000}})

//$lte - less than
db.products.find({price:{$lte:30000}})

db.users.insertMany([

    {
        firstName: "Mary Jane",
        lastName: "Watson",
        email: "mjtiger@gmail.com",
        password: "tigerjackpot15",
        isAdmin: false
    },
    {
        firstName: "Gwen",
        lastName: "Stacy",
        email: "stacyTech@gmail.com",
        password: "stacyTech1991",
        isAdmin: true
    },
    {
        firstName: "Peter",
        lastName: "Parker",
        email: "peterWebDev@gmail.com",
        password: "webdeveloperpeter",
        isAdmin: true
    },
    {
        firstName: "Jonah",
        lastName: "Jameson",
        email: "jjjameson@gmail.com",
        password: "spideyisamenace",
        isAdmin: false
    },
    {
        firstName: "Otto",
        lastName: "Octavius",
        email: "ottoOctopi@gmail.com",
        password: "docOck25",
        isAdmin: true
    }
    
])


//$regex - query operator which allow us to find documents which will mmatch charters we are looking for.


//$regex to look for documents with a partial mtch and by default is case sensitive 
db.users.find({firstName: {$regex: 'O'}})

//$options: $i -  will make our regex case insensitive

db.users.find({firstName: {$regex: 'E',$options: '$i'}})

//We can also use $regex for partial matches
//db.products.find({name:{$regex: 'phone', $options:'$i'}})
//db.users.find({email:{$regex: 'web', $options:'$i'}})
//db.products.find({name:{$regex: 'razer', $options:'$i'}})
db.products.find({name:{$regex: 'rakk', $options:'$i'}})


//$or $and - logical operators and they work almost the same as in JS 
//$or - allows us to set conditions to find for documents which can satisfy at least 1 of the given conditions.

//db.products.find({$or:[{name:{$regex: 'x', $options: 'i'}},{price:{$lte:10000}}]})

db.products.find({
        $or: [
                
            {name:{$regex: 'x',$options: '$i'}},
            {price:{$gte:30000}}
            ]
            
    })

//$and - look or find documents that satisfies both conditions:
db.products.find({
        $and: [
                
            {name:{$regex: 'razer',$options: '$i'}},
            {price:{$gte:3000}}
            ]
            
    })

db.users.find({
        $and: [
                
            {lastName:{$regex: 'a',$options: '$i'}},
            {isAdmin: true}
            ]
            
    })

//Field Projection - allows us to hide/show certain fields and properties of documents that were retrieved.

//db.users.find({},{_id:0,password:0})

//Field Projection = 0 means hide, 1 means show
//By default we have to explicitly hide the id field
//db.users.find({isAdmin:true},{_id:0,email:1})

db.users.find({isAdmin:false},{firstName:1,lastName:1})

















